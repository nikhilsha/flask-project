import tkinter as tk
# from tkinter import colorchooser
root = tk.Tk()
root.geo = root.geometry
root.geo('1000x750')
text = tk.Text(root)
text.place(x=150, y=0, width=900, height=750)


def extend():
    text.insert(1.0, '{% extends "BASE FILE NAME" %}\n')


def style():
    text.insert(
        tk.CURRENT, '''{% block stylesheet %}\n<link rel="stylesheet" href="{{  url_for("static", filename="CSS FILE")  }}">\n{% endblock %}''')


def urlfor():
    text.insert(tk.CURRENT, '''{{  url_for("static", filename="")  }}''')


Sidebar = tk.Frame(root, bg='#000000')
Sidebar.place(x=0, y=0, width=150, height=750)
ExtendsButton = tk.Button(Sidebar, text='Extends', command=extend)
ExtendsButton.place(x=0, y=0)
StyleBtn = tk.Button(Sidebar, text='Stylesheet Block', command=style)
StyleBtn.place(x=0, y=50)
UrlforButton = tk.Button(Sidebar, text='Add url_for()', command=urlfor)
UrlforButton.place(x=0, y=100)
# root.bind('<Key>', keycodeget)
root.mainloop()
