from flask import Flask
from flask import render_template
import dja
app = Flask(__name__)


@app.route('/')
def function():
    return render_template('html/Welcome.html')


@app.route('/dja')
def dad_joke():
    return dja.joke() + '<br><button onClick="window.location.reload();">Refresh Page</button>'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
