import ast
from flask import Flask
from flask import request
from flask import render_template

global app, app_format
app = Flask(__name__)
app_format = ['<h3 class="appear">', '</h3>', '<p class="appear">', '</p>', '''<button onclick='copy("''', '''");'>Get for Windows</button>''', '''<button onclick='copy("''',
              '''");'>Get for Mac</button>''', '<textarea width=1 height=1 class="appear" wrap="off" readonly id="', '">', '</textarea>', '<textarea width=1 height=1 class="appear" wrap="off" readonly id="', '">', '</textarea>']
apps = []
guiapps = open('templates/clientpage.html', 'r').read()
guiapps = ast.literal_eval(guiapps)
for i in range(len(guiapps)):
    apps.append(guiapps[i]['name'])


@app.route('/')
def Slash():
    return render_template('home.html')


@app.route('/apps')
def SlashApps():
    return render_template('apps.html')


@app.route('/apps/make')
def SlashAppsSlashMake():
    return render_template('appsmake.html')


@app.route('/credits')
def SlashCredits():
    return render_template('credits.html')


@app.route('/templates')
def SlashTemplates():
    return render_template('templates.html')


@app.route('/apps/make', methods=['POST'])
def SlashAppsSlaskMake2():
    global name, descrip, wincode, maccode
    name = request.form['name']
    descrip = request.form['descrip']
    wincode = request.form['wincode']
    maccode = request.form['maccode']

    def SlashApps2():
        if name == '' or descrip == '' or maccode == '' or wincode == '':
            pass
        else:
            # For Website
            apps.append(name)
            file = open('templates/apps.html', 'a')
            file.write('\n<br>')
            file.write('\n' + app_format[0] + name + app_format[1])
            file.write('\n' + app_format[2] + descrip + app_format[3])
            file.write(
                '\n' + app_format[4] + str(len(apps)) + 'w' + app_format[5])
            file.write(
                '\n' + app_format[6] + str(len(apps)) + 'm' + app_format[7])
            file.write('\n' + app_format[8] + str(len(apps)) +
                       'w' + app_format[9] + wincode + app_format[10])
            file.write('\n' + app_format[11] + str(len(apps)) + 'm' +
                       app_format[12] + maccode + app_format[13])
            file.write(
                '\n<br><p class="appear">_______________________________________</p>')
            file.close()
            # For GUI Client
            guiapps.append({'name': name, 'description': descrip,
                            'Mac Code': maccode, 'Windows Code': wincode})
            file = open('templates/clientpage.html', 'w')
            file.write(str(guiapps) + '\n')
            file.close()

    SlashApps2()
    return render_template('appsmake.html')


@app.route('/apps/help')
def SlashAppsSlashHelp():
    return render_template('appshelp.html')


@app.errorhandler(404)
def Error404(error):
    error
    return render_template('404.html')


@app.route('/favicon.ico')
def favicon():
    return app.send_static_file('favicon.ico')


@app.route('/templates/previews')
def SlashTemplatesSlashPreviews():
    return render_template('viewtemplates.html')


@app.route('/templates/downloads')
def SlashTemplatesSlashDownloads():
    return render_template('downloadtemplates.html')


@app.route('/guiclient')
def SlashGUIClient():
    return str(guiapps)


@app.route('/templates/downloads/termsofuse')
def SlashTemplatesSlashDownloadsSlashTermsOfUse():
    return render_template('downloadterms.html')


@app.errorhandler(500)
def Error500(error):
    error
    return render_template('500.html')


if __name__ == '__main__':
    app.run()
