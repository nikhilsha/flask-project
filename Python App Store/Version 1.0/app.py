import webbrowser
from flask import Flask
from flask import render_template
from flask import request

global app, app_format
app = Flask(__name__)
app_format = ['<h4>', '</h4>', '<p>', '</p>', '''<button onclick='copy("''', '''");'>Get for Windows</button>''', '''<button onclick='copy("''',
            '''");'>Get for Mac</button>''', '<textarea wrap="off" readonly id="', '">', '</textarea>', '<textarea wrap="off" readonly id="', '">', '</textarea>']
apps = []
host = '0.0.0.0'
port = 5000
if port == 'DEFAULT':
    port = 5000
if host == 'DEFAULT':
    host = '127.0.0.1'

@app.route('/')
def SLASH():
    return render_template('html/home.html')


@app.route('/apps')
def SLASHAPPS():
    return render_template('html/apps.html')


@app.route('/apps/make')
def SLASHAPPSSLASHMAKE():
    return render_template('html/appsmake.html')

@app.route('/credits')
def SLASHCREDITS():
    return render_template('html/used.html')


@app.route('/apps/make', methods=['POST'])
def SLASHAPPSSLASHMAKE2():
    global name, descrip, wincode, maccode
    name = request.form['name']
    descrip = request.form['descrip']
    wincode = request.form['wincode']
    maccode = request.form['maccode']

    def SLASHAPPS2():
        if name == '' and descrip == '' and maccode == '' and wincode == '':
            pass
        else:
            apps.append(name)
            file = open('templates/html/apps.html', 'a')
            file.write('\n<br>')
            file.write('\n' + app_format[0] + name + app_format[1])
            file.write('\n' + app_format[2] + descrip + app_format[3])
            file.write('\n' + app_format[4] + str(len(apps)) + 'w' + str(len(apps)) + app_format[5])
            file.write('\n' + app_format[6] + str(len(apps)) + 'm' + str(len(apps)) + app_format[7])
            file.write('\n' + app_format[8] + str(len(apps)) + 'w' + str(len(apps))+ app_format[9] + wincode + app_format[10])
            file.write('\n' + app_format[11] + str(len(apps)) + 'm' + str(len(apps)) + app_format[12] + maccode + app_format[13])
            file.write('\n<br>_______________________________________')
            file.close()

    SLASHAPPS2()
    return render_template('html/appsmake.html')


@app.route('/apps/help')
def SLASHAPPSSLASHHELP():
    return render_template('html/appshelp.html')

webbrowser.open('http://' + host + ':' + str(port))
app.run(debug=True, host=host, port=port)
